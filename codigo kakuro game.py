# en esta variable esta la cantidad de casillas usables
# y los valores producibles en las sumas de estos
# esto es una hueristica para limpiar los dominios ya que hay valores que con suma de datos
# no darian ejemplo 2 casillas sumar 20
domains = {
     2: {
          3: { 1, 2},
          4: { 1, 3},
          5: { 1, 2, 3, 4},
          6: { 1, 2, 4, 5},
          7: { 1, 2, 3, 4, 5, 6},
          8: { 1, 2, 3, 5, 6, 7},
          9: { 1, 2, 3, 4, 5, 6, 7, 8},
          10: { 1, 2, 3, 4, 6, 7, 8, 9},
          11: { 2, 3, 4, 5, 6, 7, 8, 9},
          12: { 3, 4, 5, 7, 8, 9},
          13: { 4, 5, 6, 7, 8, 9},
          14: { 5, 6, 8, 9},
          15: { 6, 7, 8, 9},
          16: { 7, 9},
          17: { 8, 9}
     }
     ,
     3: {
          6: { 1, 2, 3},
          7: { 1, 2, 4},
          8: { 1, 2, 3, 4, 5},
          9: { 1, 2, 3, 4, 5, 6},
          10: { 1, 2, 3, 4, 5, 6, 7},
          11: { 1, 2, 3, 4, 5, 6, 7, 8},
          12: { 1, 2, 3, 4, 5, 6, 7, 8, 9},
          13: { 1, 2, 3, 4, 5, 6, 7, 8, 9},
          14: { 1, 2, 3, 4, 5, 6, 7, 8, 9},
          15: { 1, 2, 3, 4, 5, 6, 7, 8, 9},
          16: { 1, 2, 3, 4, 5, 6, 7, 8, 9},
          17: { 1, 2, 3, 4, 5, 6, 7, 8, 9},
          18: { 1, 2, 3, 4, 5, 6, 7, 8, 9},
          19: { 2, 3, 4, 5, 6, 7, 8, 9},
          20: { 3, 4, 5, 6, 7, 8, 9},
          21: { 4, 5, 6, 7, 8, 9},
          22: { 5, 6, 7, 8, 9},
          23: { 6, 8, 9},
          24: { 7, 8, 9}
     }
     ,
     4: {
          10: { 1, 2, 3, 4},
          11: { 1, 2, 3, 5},
          12: { 1, 2, 3, 4, 5, 6},
          13: { 1, 2, 3, 4, 5, 6, 7},
          14: { 1, 2, 3, 4, 5, 6, 7, 8},
          15: { 1, 2, 3, 4, 5, 6, 7, 8, 9},
          16: { 1, 2, 3, 4, 5, 6, 7, 8, 9},
          17: { 1, 2, 3, 4, 5, 6, 7, 8, 9},
          18: { 1, 2, 3, 4, 5, 6, 7, 8, 9},
          19: { 1, 2, 3, 4, 5, 6, 7, 8, 9},
          20: { 1, 2, 3, 4, 5, 6, 7, 8, 9},
          21: { 1, 2, 3, 4, 5, 6, 7, 8, 9},
          22: { 1, 2, 3, 4, 5, 6, 7, 8, 9},
          23: { 1, 2, 3, 4, 5, 6, 7, 8, 9},
          24: { 1, 2, 3, 4, 5, 6, 7, 8, 9},
          25: { 1, 2, 3, 4, 5, 6, 7, 8, 9},
          26: { 2, 3, 4, 5, 6, 7, 8, 9},
          27: { 3, 4, 5, 6, 7, 8, 9},
          28: { 4, 5, 6, 7, 8, 9},
          29: { 5, 7, 8, 9},
          30: { 6, 7, 8, 9}
     },
     5: {
          15: { 1, 2, 3, 4, 5},
          16: { 1, 2, 3, 4, 6},
          17: { 1, 2, 3, 4, 5, 6, 7},
          18: { 1, 2, 3, 4, 5, 6, 7, 8},
          19: { 1, 2, 3, 4, 5, 6, 7, 8, 9},
          20: { 1, 2, 3, 4, 5, 6, 7, 8, 9},
          21: { 1, 2, 3, 4, 5, 6, 7, 8, 9},
          22: { 1, 2, 3, 4, 5, 6, 7, 8, 9},
          23: { 1, 2, 3, 4, 5, 6, 7, 8, 9},
          24: { 1, 2, 3, 4, 5, 6, 7, 8, 9},
          25: { 1, 2, 3, 4, 5, 6, 7, 8, 9},
          26: { 1, 2, 3, 4, 5, 6, 7, 8, 9},
          27: { 1, 2, 3, 4, 5, 6, 7, 8, 9},
          28: { 1, 2, 3, 4, 5, 6, 7, 8, 9},
          29: { 1, 2, 3, 4, 5, 6, 7, 8, 9},
          30: { 1, 2, 3, 4, 5, 6, 7, 8, 9},
          31: { 1, 2, 3, 4, 5, 6, 7, 8, 9},
          32: { 2, 3, 4, 5, 6, 7, 8, 9},
          33: { 3, 4, 5, 6, 7, 8, 9},
          34: { 4, 5, 6, 7, 8, 9},
          35: { 5, 6, 7, 8, 9}
     },
     6: {
          21: { 1, 2, 3, 4, 5, 6},
          22: { 1, 2, 3, 4, 5, 7},
          23: { 1, 2, 3, 4, 5, 6, 7, 8},
          24: { 1, 2, 3, 4, 5, 6, 7, 8, 9},
          25: { 1, 2, 3, 4, 5, 6, 7, 8, 9},
          26: { 1, 2, 3, 4, 5, 6, 7, 8, 9},
          27: { 1, 2, 3, 4, 5, 6, 7, 8, 9},
          28: { 1, 2, 3, 4, 5, 6, 7, 8, 9},
          29: { 1, 2, 3, 4, 5, 6, 7, 8, 9},
          30: { 1, 2, 3, 4, 5, 6, 7, 8, 9},
          31: { 1, 2, 3, 4, 5, 6, 7, 8, 9},
          32: { 1, 2, 3, 4, 5, 6, 7, 8, 9},
          33: { 1, 2, 3, 4, 5, 6, 7, 8, 9},
          34: { 1, 2, 3, 4, 5, 6, 7, 8, 9},
          35: { 1, 2, 3, 4, 5, 6, 7, 8, 9},
          36: { 1, 2, 3, 4, 5, 6, 7, 8, 9},
          37: { 3, 4, 5, 6, 7, 8, 9},
          38: { 3, 4, 5, 6, 7, 8, 9},
          39: { 4, 5, 6, 7, 8, 9}
     }
     ,
     7: {
          28: { 1, 2, 3, 4, 5, 6, 7},
          29: { 1, 2, 3, 4, 5, 6, 8},
          30: { 1, 2, 3, 4, 5, 6, 7, 8, 9},
          31: { 1, 2, 3, 4, 5, 6, 7, 8, 9},
          32: { 1, 2, 3, 4, 5, 6, 7, 8, 9},
          33: { 1, 2, 3, 4, 5, 6, 7, 8, 9},
          34: { 1, 2, 3, 4, 5, 6, 7, 8, 9},
          35: { 1, 2, 3, 4, 5, 6, 7, 8, 9},
          36: { 1, 2, 3, 4, 5, 6, 7, 8, 9},
          37: { 1, 2, 3, 4, 5, 6, 7, 8, 9},
          38: { 1, 2, 3, 4, 5, 6, 7, 8, 9},
          39: { 1, 2, 3, 4, 5, 6, 7, 8, 9},
          40: { 1, 2, 3, 4, 5, 6, 7, 8, 9},
          41: { 2, 4, 5, 6, 7, 8, 9},
          42: { 3, 4, 5, 6, 7, 8, 9}
     }
     ,
     8: {
          36: { 1, 2, 3, 4, 5, 6, 7, 8},
          37: { 1, 2, 3, 4, 5, 6, 7, 9},
          38: { 1, 2, 3, 4, 5, 6, 8, 9},
          39: { 1, 2, 3, 4, 5, 7, 8, 9},
          40: { 1, 2, 3, 4, 6, 7, 8, 9},
          41: { 1, 2, 3, 5, 6, 7, 8, 9},
          42: { 1, 2, 4, 5, 6, 7, 8, 9},
          43: { 1, 3, 4, 5, 6, 7, 8, 9},
          44: { 2, 3, 4, 5, 6, 7, 8, 9}
     }
     ,
     9: {
          45: { 1, 2, 3, 4, 5, 6, 7, 8, 9}
     }
}

# inicializa el tablero colocando el rango total posible en cada casilla
def clearBoard(Vars):
  for row in rows:
    for col in cols:
      Vars[f"{col}{row}"]=rows.copy()

# lee el tablero dado en un archivo el cual se da por el nombre
# lee cada una de las lineas y las pone en la secuencia de
# posiciones siempre y cuando sea un valor unico
# en el boar 
# las posiciones sin valor se dan como 0
# las sumas buscadas con un nuero negativo consecutivo
# cuando es un valor a buscar se puede poner un numero x de dos digitos o mas (a desicion)
def loadBoard(Vars, filename):
  global menor
  with open(filename, 'r') as file:
    for row in rows:
      for col in cols:
        value = int(file.readline())
        if (value < 10):
          Vars[f"{col}{row}"]={value}
          if value < menor:
            menor = value

# ESTA FUNCION SE ENCARGA DE CARGAR LAS RESTRICCIONES APLICADAS A EL TABLERO 
# CARGADO, ES DECIR LOS VALORES DE SUMAS BUSCADOS
# aca se debe colocar restriccion por linea asi 
# a,#,d,#
# a = abajo
# d = derecha
# no es necesario poner a y d, solo la que se utilice en el tablero
def loadSum(filename):
  # Crear un diccionario para almacenar los diccionarios de cada línea numerados
  data_dict = {}

  # Abrir el archivo para lectura
  with open(filename, 'r') as file:

    # Leer cada línea del archivo con un índice
    for line_number, line in enumerate(file, start=1):

      # Crear un diccionario para la línea actual
      line_dict = {}
      # Separar la línea en partes
      parts = line.strip().split(',')

      keys = []
      for part in parts:

        if part.isalpha():
          keys.append(part)
        elif part.isdigit():
          value = int(part)
          for key in keys:
            if key in line_dict:
              line_dict[key].append(value)
            else:
              line_dict[key] = [value]
            keys = []  # Reiniciar las claves después de asignar valores
        elif part.replace('.', '', 1).isdigit():  # manejar casos como 'd.4'
          value = float(part)  # usar float en lugar de int si se permiten decimales
          for key in keys:
            if key in line_dict:
              line_dict[key].append(value)
            else:
              line_dict[key] = [value]
            keys = []  # Reiniciar las claves después de asignar valores

      # Agregar el diccionario de la línea al diccionario principal con el número de línea como clave
      data_dict[line_number] = line_dict

  return data_dict

# esta funcion se encarga de realizar la impresion del tablero
# en una forma mas identificable, es decir en forma de cuadro
def showFormatBoard(vars):

  cols = '123456789'
  rows = 'ABCDEFGHI'

  print("-----------------------")
  for col in cols:
    linea = ""
    for row in rows:
      if len(vars[f"{row}{col}"]) == 1:
        if str(list(vars[f"{row}{col}"]))[1] == "0":
          linea = linea +  "# "
        else:
          linea = linea + str(list(vars[f"{row}{col}"]))[1] + " "
      else :
        linea = linea + "0 "
      if row == 'C' or row == 'F':
        linea = linea + ""
    print(linea)

  print("-----------------------")

# ESTA FUNCION SE ENCARGA DE REALIZAR LA ORGANIZACION DE LOS DOMINIOS
# POSIBLES PARA EL VALOR DE LA SUMA INDICADA
# PARA LAS COLUMNAS
def organizarDominioAbajo(coordenada, Vars, ValorBuscado):

  # saca fila y columna inicial
  fila = int(coordenada[1:])
  columna = coordenada[0]

  ubicaciones = []
  posicionesDisponibles = 0
  # loop para contar casillas y obtener lass posiciones a usar
  while True:
    if (fila < 9):
      if (len(Vars[columna+str(fila+1)]) == 1 and next(iter(Vars[columna+str(fila+1)])) < 0):
        break
      else :
        if (next(iter(Vars[columna+str(fila+1)])) > 0):
          posicionesDisponibles += 1
          fila += 1
          ubicaciones.append(columna+str(fila))
        else:
          break
    else:
      break
  const.append(ubicaciones)
  constAux.append((ValorBuscado, set(ubicaciones.copy())))
  # se obtiene el valor a usar para los dominios a
  a = domains[posicionesDisponibles]
  a = a[ValorBuscado]

  # # se iteran las posiciones para cambiar el dominio
  for key in ubicaciones:
    valores_iguales  = set()
    for val in a.copy():
      if val in Vars[key]:
        valores_iguales.add(val)
    Vars[key] = valores_iguales

# ESTA FUNCION SE ENCARGA DE REALIZAR LA INSERCION DE LOS DOMINIOS
# PARA LAS FILAS SEGUN LA SUMA QUE SE INDICA DEBE OBTENER
# COMO RESULTADO 
def organizarDominioDerecha(coordenada, Vars, ValorBuscado):
  # saca fila y columna inicial
  fila = int(coordenada[1:])
  columna = coordenada[0]

  # Define el límite de columnas ('A' a 'I')
  limite_columna = 'I'

  ubicaciones = []
  posicionesDisponibles = 0
  # loop para contar casillas y obtener lass posiciones a usar
  while True:
    # Verifica si la columna está dentro del límite
    if (ord(columna) < ord(limite_columna)):
      # Verifica si la casilla tiene un valor negativo (casilla bloqueada)
      if (len(Vars[chr(ord(columna)+1)+str(fila)]) == 1 and next(iter(Vars[chr(ord(columna)+1)+str(fila)])) < 0):
        break
      else:
        # Verifica si la casilla tiene un valor positivo (casilla disponible)
        if (next(iter(Vars[chr(ord(columna)+1)+str(fila)])) > 0):
          posicionesDisponibles += 1
          columna = chr(ord(columna) + 1)
          ubicaciones.append(columna+str(fila))
        else:
          break
    else:
      break
  const.append(ubicaciones)
  constAux.append((ValorBuscado, set(ubicaciones.copy())))
  # se obtiene el valor a usar para los dominios b
  a = domains[posicionesDisponibles]
  a = a[ValorBuscado]

  # # se iteran las posiciones para cambiar el dominio
  for key in ubicaciones:
    valores_iguales  = set()
    for val in a.copy():
      if val in Vars[key]:
        valores_iguales.add(val)
    Vars[key] = valores_iguales

# esta funcion compara los dos diccionarios para organizar los dominios que se
# van a usar para el kakuro
def compare_and_validate(dict1, dict2):
  # itera el diccionario del board
  for key1, values1 in dict1.items():
    # solo itera los valores que son de un solo dominio
    if (len(values1) == 1) :
      # saca el valor del dominio
      value1 = next(iter(values1))
      # solo itera si el dominio es negativo, ya que estos negativos
      # representan las sumas buscadas
      if (value1 < 0) :
        # toma el valor absoluto del numero de la suma buscada
        abs_value1 = abs(value1)
        # itera el array de los valores suma buscados
        for key2, values2 in dict2.items():
          # solo itera lo interno si es la suma buscada asociada al negativo
          if key2 == abs_value1:
            # itera el diccionario asociado a la suma buscada
            for key2_inner, values2_inner in values2.items():
              # valida si es a (abajo)
              if key2_inner == 'a':
                organizarDominioAbajo(key1, dict1, next(iter(values2_inner)))
              # valida si es d (derecha)
              if key2_inner == 'd':
                organizarDominioDerecha(key1, dict1, next(iter(values2_inner)))

# ESTA FUNCION VALIDA QUE NO SE REPITAN NUMEROS UNICOS  
# POR DOMINIO
def allDif(Vars, varList):
  anyChange = False
  for var1 in varList:
    if (len(Vars[var1]) == 1 ):
      if (next(iter(Vars[var1])) > 0) :
        for var2 in varList:
          if (not(var1 == var2)):
            oldDom = Vars[var2].copy()
            Vars[var2].discard(list(Vars[var1])[0])
            if (not(oldDom == Vars[var2])):
              anyChange = True
  return anyChange


# ESTA FUNCION SE ENCARGA DE VALIDAR DONDE SE TIENEN DOMINIOS IGUALES 
# Y LIMPIA ESTOS DE LAS DEMAS CASILLAS ASOCIADAS 
def DomsEquals(Vars,constraint):
  anyChange=False
  varsEquals={}
  for var1 in constraint:
    if len(Vars[var1])>1:
      for var2 in constraint:
        if not(var1==var2):
          if (Vars[var1]==Vars[var2]):
            if tuple(Vars[var1]) in varsEquals:
              Set_aux=set(varsEquals[tuple(Vars[var1])].copy())
              Set_aux.add(var1)
              Set_aux.add(var2)
              varsEquals[tuple(Vars[var1])]=list(Set_aux)
            else:
              varsEquals[tuple(Vars[var1])]=[var1,var2]
  for domVar in varsEquals:
    if len(domVar)==len(varsEquals[domVar]):
      for var in constraint:
        if not(var in varsEquals[domVar]):
          for value in domVar:
            oldValue=Vars[var].copy()
            Vars[var].discard(value)
            if not(oldValue==Vars[var]):
              anyChange=True
  return anyChange

# ESTA FUCNION SE ENCARGA DE REALIZAR EL CALCULO RAPIDO DE SOLUCION PARA 
# DOMINIOS EN LOS CUALES SOLO FALTA UN DATO PARA COMPLETAR EL VALOR
# ESTO SE BASA EN REGLAS Y RESTRICCIONES PROPIAS DE LA SUMA
def valor_rapido_n(Vars, n):
  anyChange = False

  for clave, valores in constAux:
    if len(valores) == n:
      len_list = [len(Vars[var]) for var in valores]

      # Encuentra las variables con longitud 1 y aquellas con longitud mayor que 1
      single_vars = [var for var, l in zip(valores, len_list) if l == 1]
      multi_vars = [var for var, l in zip(valores, len_list) if l > 1]

      # Si hay exactamente una variable con longitud mayor que 1 y todas las demás con longitud 1
      if len(multi_vars) == 1 and len(single_vars) == n - 1:
        multi_var = multi_vars[0]
        sum_single_values = sum(next(iter(Vars[var])) for var in single_vars)
        Vars[multi_var] = {clave - sum_single_values}
        anyChange = True

  return anyChange

# ESTA FUNCION SE ENCARGA DE REALIZAR EL LLAMADO A VALOR RAPIDO PARA LOS 
# VALORES POSIBLES
def actualizar_valores(Vars):
    anyChanges = any(valor_rapido_n(Vars, n) for n in range(2, 9))
    return anyChanges

# ESTA FUNCION LIMPIA EL DOMINIO BASE
def limpiarDominio(Vars):
  anyChanges = True
  while anyChanges:
    anyChanges = False
    for varList in const:
      anyChanges = allDif(Vars, varList) if (not(anyChanges)) else True
      anyChanges = DomsEquals(Vars, varList) if not(anyChanges) else True
      actualizar_valores(Vars)

# ESTA ES LA FUNCION ENCARGA DE REALIZAR LA SOLUCION DE KAKURO LUEGO DE APLICAR LAS
# RESTICCIONES Y LIMPIAR EL DOMINO POSIBLE DE SOLUCION
def solve_kakuro(vars):

  # saca las pocisiones que tienen mas de un valor en el dominio
  empty_positions = [pos for pos in vars if len(vars[pos]) > 1]

  # # si no hay posiciones con mas de un valor de dominio finaliza
  if not empty_positions:
    return True

  # inicializa las variables para obtener la posicion con menor cantidad de
  # datos en el domino
  min_candidates = float('inf')
  min_position = None

  # en este loop se iteran las posiciones encontradas anteriormente para
  # determinar cual es la posicion del dominio con menor longitud
  for pos in empty_positions:
    num_candidates = len(vars[pos])
    if num_candidates < min_candidates:
        min_candidates = num_candidates
        min_position = pos

  # print(min_position)

  # en esta funcion se itera el dominio
  for value in list(vars[min_position]):

    # aca se valida si el movimiento a realizar es valido
    if is_valid_move(value, min_position, vars):

      # se realiza una copia de los dominios
      vars_copy = vars.copy()
      # se realiza el cambio del valor del dominio en la posicion encontrada
      vars_copy[min_position] = {value}
      # se vuelve a llamar de forma recursiva con el nuevo dominio
      if solve_kakuro(vars_copy):
        # si el solucionador se completa limipa la variable original
        vars.clear()
        # agrega el valor de los dominios encontrados
        vars.update(vars_copy)
        # retorna un true al solucionarlo
        return True

  # retorna un false por no solucion
  return False

# ESTA FUNCION SE ENCARGA DE VALIDAR LA NO REPETICION DEL VALOR POR FILA Y COLUMNA
def funAux1(value, pos, vars):
  # itera todas las restricciones generadas
  for group in const:
    # itera cada una las restricciones generadas
    if pos in group:
      # itera componente de la restriccion
      for component in group:
        # valida si se verifica en una celda diferente a la que se intenta validar
        # verifica si ya es una celda solucionada
        # verifica si el valor de la celda es el mismo validado
        if component != pos and len(vars[component]) == 1 and list(vars[component])[0] == value:
          # si se da algo de lo anterior el movimiento no es valido
          return False
  # en caso de que no se encuentre error se da como movimiento valido        
  return True

# Función para validar si un movimiento es válido en una posición
# Y SI LA SUMA ENTREGADA ES VALIDA 
def is_valid_move(value, pos, vars):
  
  retorno = True

  retorno = funAux1(value, pos, vars)

  if not retorno:
    return retorno

  # valida si el campo es valido para suma si ya solo falta ese valor para el campo
  for clave, valores in constAux:
    # valida que la posicion este en la restriccion solo para esos valores
    if pos in valores:
      # print("-----------------")
      # print(pos)
      # saca una copia del arreglo y descarta el valor de poscion buscada
      dominio = valores.copy()
      dominio.discard(pos)

      # print(dominio)

      # indica que se valide la suma
      validarSuma = True
      # se iteran las posiciones del dominio para validar si todos ya son de un solo "digito"
      for dato in dominio:
        # print(dato)
        # print(vars[dato])
        if len(vars[dato]) != 1:
          validarSuma = False
          break
      # print(validarSuma)

      # si se debe validar la suma
      if validarSuma:
        suma = value
        # se iteran los datos de las posciones
        for dato in dominio:
          suma += sum(vars[dato])

        # print(suma)
        if not suma == clave:
          return False
      # print("------------------")

  # en caso de que no se encuentre error se da como movimiento valido
  return True

# LLAMADO PARA EJECUCION
menor = 0
cols="ABCDEFGHI"
rows=set(range(1,10))

Vars={}
const = []
constAux = []

clearBoard(Vars)
loadBoard(Vars, 'board')
Sums=loadSum('resticciones')

compare_and_validate(Vars, Sums)

limpiarDominio(Vars)

a = Vars.copy()
solve_kakuro(a)

showFormatBoard(a)